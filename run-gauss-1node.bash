#!/bin/bash

cd /home/lwang124/projects/iit-cs546-assignment4/run-gauss/

echo "Current working dir:"
pwd
echo " "

echo "Begin to run program:"

echo "-------------------------------------------------"
echo "CORRECTNESS - num_proc = 1, 2, 4 and 8"
echo "-------------------------------------------------"
mpirun -npernode 1 ../build/gauss_mpi 10 0
mpirun -npernode 2 ../build/gauss_mpi 10 0
mpirun -npernode 4 ../build/gauss_mpi 10 0
mpirun -npernode 8 ../build/gauss_mpi 10 0

echo "-------------------------------------------------"
echo "PERFORMANCE - num_proc = 1"
echo "-------------------------------------------------"
mpirun -npernode 1 ../build/gauss_mpi 2000 0

echo "-------------------------------------------------"
echo "PERFORMANCE - num_proc = 2"
echo "-------------------------------------------------"
mpirun -npernode 2 ../build/gauss_mpi 2000 0

echo "-------------------------------------------------"
echo "PERFORMANCE - num_proc = 4"
echo "-------------------------------------------------"
mpirun -npernode 4 ../build/gauss_mpi 2000 0

echo "-------------------------------------------------"
echo "PERFORMANCE - num_proc = 8"
echo "-------------------------------------------------"
mpirun -npernode 8 ../build/gauss_mpi 2000 0

echo "-------------------------------------------------"
echo "PERFORMANCE - num_proc = 12"
echo "-------------------------------------------------"
mpirun -npernode 12 ../build/gauss_mpi 2000 0

echo "-------------------------------------------------"
echo "PERFORMANCE - num_proc = 16"
echo "-------------------------------------------------"
mpirun -npernode 16 ../build/gauss_mpi 2000 0


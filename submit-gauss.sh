#!/bin/sh

qsub -P cs546_s14_project -q nvidia_480.q -pe mpich 1 run-gauss-1node.bash 
qsub -P cs546_s14_project -q nvidia_480.q -pe mpich 2 run-gauss-2nodes.bash
qsub -P cs546_s14_project -q nvidia_480.q -pe mpich 4 run-gauss-4nodes.bash

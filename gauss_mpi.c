// ========================================================
// filename : gauss_mpi.c
// author   : Gates Wong
// date     : 2014-04-09
// description :
//     Gaussian elimination. With OpenMPI
// ========================================================

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <string.h>

#include <sys/types.h>
#include <sys/times.h>
#include <sys/time.h>
#include <time.h>

#include <mpi.h>

// Program Parameters
#define MAXN 2000			// Maximum Martix Size
int N;						// Given Matrix Size

// Matrices and Vectors
float A[MAXN][MAXN], B[MAXN], X[MAXN];

// Prototype
void gauss();
void backward();

// MPI Variables
int my_rank, num_proc;

// returns a seed for srand based on the time
unsigned int time_seed() {
	struct timeval t;
	struct timezone tzdummy;

	gettimeofday(&t, &tzdummy);
	return (unsigned int)(t.tv_usec);
}

/* Set the program parameters from the command-line arguments */
void parameters(int argc, char **argv) {
	int seed = 0;  /* Random seed */
	char uid[32]; /*User name */
	unsigned int timeseed = time_seed();
	int ii;

	/* Read command-line arguments */
	if (0 == my_rank) {
		for (ii = 1; ii < num_proc; ii ++) {
			if (1 == num_proc) { break; }
			MPI_Send(&timeseed, 1, MPI_INT, ii, 0, MPI_COMM_WORLD);
		}
	} else {
		MPI_Status status;
		MPI_Recv(&timeseed, 1, MPI_INT, ii, 0, MPI_COMM_WORLD, &status);
	}
	srand(timeseed);  /* Randomize */
	seed = atoi(argv[2]);
	srand(seed);

	if (argc == 3) {
		if (0 == my_rank) {
			printf("Random seed = %i\n", seed);
			for (ii = 1; ii < num_proc; ii ++) {
				if (num_proc == 1) { break; }
				MPI_Send(&seed, 1, MPI_INT, ii, 0, MPI_COMM_WORLD);
			}
		} else {
			MPI_Status status;
			MPI_Recv(&seed, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
		}
	} 
	if (argc >= 2) {
		N = atoi(argv[1]);
		if (N < 1 || N > MAXN) {
			if (0 == my_rank) { printf("N = %i is out of range.\n", N); }
			exit(0);
		}
	}
	else {
		printf("Usage: %s <matrix_dimension> [random seed]\n",
					 argv[0]);    
		exit(0);
	}

	/* Print parameters */
	if (0 == my_rank) { printf("\nMatrix dimension N = %i.\n", N); }
}

/* Initialize A and B (and X to 0.0s) */
void initialize_inputs() {
	int row, col;

	if (0 == my_rank) { printf("\nInitializing...\n"); }
	for (col = 0; col < N; col++) {
		for (row = 0; row < N; row++) {
			A[row][col] = (float)rand() / 32768.0;
		}
		B[col] = (float)rand() / 32768.0;
		X[col] = 0.0;
	}

}

/* Print input matrices */
void print_inputs() {
	int row, col;
	if (my_rank > 0) { return; }

	if (N < 10) {
		printf("process %d ~ \n", my_rank);
		printf("\nA =\n\t");
		for (row = 0; row < N; row++) {
			for (col = 0; col < N; col++) {
				printf("%5.2f%s", A[row][col], (col < N-1) ? ", " : ";\n\t");
			}
		}
		printf("\nB = [");
		for (col = 0; col < N; col++) {
			printf("%5.2f%s", B[col], (col < N-1) ? "; " : "]\n");
		}
	}
}

void print_X() {
	int row;

	if (N < 100) {
		printf("\nX = [");
		for (row = 0; row < N; row++) {
			printf("%5.2f%s", X[row], (row < N-1) ? "; " : "]\n");
		}
	}
}


#define NODEBUG

int main(int argc, char **argv) {
	double timer[10];

	// MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

	// pre-process
	parameters(argc, argv);
	initialize_inputs();
	print_inputs();

	// MPI
	timer[0] = MPI_Wtime();

	// Gaussian Elimination
	timer[1] = MPI_Wtime();
	gauss();
	if (0 == my_rank) { backward(); }
	timer[2] = MPI_Wtime();

	// MPI
	MPI_Finalize();

	// display output
	if (0 == my_rank) {
		print_X();
		// TODO report timer
		printf("The whole gaussian elimination costs %f ms.\n", 1000 * (timer[2] - timer[1]));
	}


	return 0;
}

// Gaussian Elimination
void gauss() {
	float* buffer;
	int buffersize = sizeof(float) * (N + 1); 
	buffer = malloc(buffersize);

	int norm, dest, row, ii;
	MPI_Status status;

	for(norm = 0; norm < N; norm ++) {	// norm
		if (num_proc > 1) { 	// if only one process, skip data passing.
			// Data Passing
			if (norm % num_proc == my_rank) {
				memcpy(buffer, A[norm], buffersize - sizeof(float));	// CKECK
				buffer[N] = B[norm];
				for (dest = 0; dest < num_proc; dest ++) {
					if (dest == my_rank) { continue; }
					MPI_Send(buffer, N + 1, MPI_INT, dest, norm, MPI_COMM_WORLD);
				}
			} else {
				MPI_Recv(buffer, N + 1, MPI_INT, norm % num_proc, norm, MPI_COMM_WORLD, &status);
			}
		} else {
			memcpy(buffer, A[norm], buffersize - sizeof(float));
			buffer[N] = B[norm];
		}


		// Calculation of matrix
		for (row = norm - norm % num_proc + my_rank; row < N; row += num_proc) {
			if (row < norm + 1) { continue; }
			float multiplier = A[row][norm] / buffer[norm];
			for (ii = norm; ii < N; ii ++) {
				A[row][ii] -= buffer[ii] * multiplier;
			}
			B[row] -= buffer[N] * multiplier;
		}
	}

	if (1 == num_proc) { return; }
	// Send back to process 0
	if (0 == my_rank) {
		for (ii = 1; ii < N; ii ++) {
			if (0 == ii % num_proc) { continue; }
			MPI_Recv(buffer, N + 1, MPI_INT, ii % num_proc, ii, MPI_COMM_WORLD, &status);	// Receive data
			memcpy(A[ii], buffer, buffersize - sizeof(float));	// Write data to A and B
			B[ii] = buffer[N];
		}
	} else {
		for (ii = my_rank; ii < N; ii += num_proc) {
			memcpy(buffer, A[ii], buffersize - sizeof(float));	// Read A and B to buffer
			buffer[N] = B[ii];
			MPI_Send(buffer, N + 1, MPI_INT, 0, ii, MPI_COMM_WORLD);	// Send data
		}
	}
}

// Backward Substitution
void backward() {
	int row, col;
	for (row = N - 1; row >= 0; row --) {
		X[row] = B[row];
		for (col = N - 1; col > row; col --) {
			X[row] -= A[row][col] * X[col];
		}
		X[row] /= A[row][row];
	}
}
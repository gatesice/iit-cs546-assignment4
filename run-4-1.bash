#!/bin/bash

cd /home/lwang124/projects/iit-cs546-assignment4/run-4-1

echo "Current working dir:"
pwd

echo " "
echo " "

echo "Begin to run program:"
echo "---------------------------------------"
echo " "
echo " "
mpirun -npernode 8 ../build/get_data

echo " "
echo " "
echo "---------------------------------------"
echo "Moving results..."
mv /home/lwang124/run-4-1.bash.* ./
